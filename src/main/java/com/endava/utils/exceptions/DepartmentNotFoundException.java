package com.endava.utils.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class DepartmentNotFoundException extends RuntimeException{
}
