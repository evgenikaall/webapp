package com.endava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
public class DemoApplication{
    public static void main(String[] args) {
        final ConfigurableApplicationContext run = SpringApplication.run(DemoApplication.class, args);
        System.out.println();
    }

}
