package com.endava.service;

import com.endava.model.dto.DepartmentDTO;
import com.endava.model.dto.ExpandedDepartmentDTO;
import com.endava.model.dto.TruncatedDepartmentDTO;
import com.endava.model.entity.Department;
import com.endava.repository.CRUDRepository;
import com.endava.utils.exceptions.DepartmentNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Service
@RequiredArgsConstructor
public class DepartmentService {

    private final CRUDRepository<Department, Long> repository;
    private final Converter<Department, TruncatedDepartmentDTO> EntityToTruncatedDTOConverter;
    private final Converter<Department, ExpandedDepartmentDTO> EntityToDTOConverter;
    private final Converter<ExpandedDepartmentDTO, Department> DTOToEntityConverter;

    public DepartmentDTO save(ExpandedDepartmentDTO expandedDepartmentDTO, Long id) {
        final Department department = DTOToEntityConverter.convert(expandedDepartmentDTO);
        if(nonNull(id)) department.setId(id);
        return EntityToDTOConverter.convert(repository.save(department));
    }

    public DepartmentDTO findDepartmentDTOById(Long id) {
        final Department department = repository.findById(id).orElseThrow(DepartmentNotFoundException::new);
        return EntityToDTOConverter.convert(department);
    }

    public DepartmentDTO findTruncatedDepartmentDTOById(Long id){
        final Department department = repository.findById(id).orElseThrow(DepartmentNotFoundException::new);
        return EntityToTruncatedDTOConverter.convert(department);
    }

    public List<DepartmentDTO> findAll() {
        return repository.findAll().stream().map(EntityToDTOConverter::convert).collect(Collectors.toList());
    }

    public void deleteById(Long id) {
        // on fail throw DepartmentNotFoundException
        findDepartmentDTOById(id);
        repository.deleteById(id);
    }
}
