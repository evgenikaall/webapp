package com.endava.service;

import com.endava.model.dto.EmployeeDTO;
import com.endava.model.dto.ExpandedEmployeeDTO;
import com.endava.model.dto.TruncatedEmployeeDTO;
import com.endava.model.entity.Department;
import com.endava.model.entity.Employee;
import com.endava.repository.CRUDRepository;
import com.endava.utils.exceptions.EmployeeNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Service
@RequiredArgsConstructor
public class EmployeeService {

    private final Converter<ExpandedEmployeeDTO, Employee> DTOToEntityConverter;
    private final Converter<Employee, TruncatedEmployeeDTO> EntityToTruncatedDTOConverter;
    private final Converter<Employee, ExpandedEmployeeDTO> EntityToDTOConverter;
    private final CRUDRepository<Employee, Long> employeeRepository;

    private final DepartmentService departmentService;

    public EmployeeDTO save(ExpandedEmployeeDTO expandedEmployeeDTO, Long id) {
        final Department departmentFromDTO = expandedEmployeeDTO.getDepartment();

        // on failure throw DepartmentNotFoundException
        departmentService.findDepartmentDTOById(departmentFromDTO.getId());

        final Employee employee = DTOToEntityConverter.convert(expandedEmployeeDTO);
        if (nonNull(id)) employee.setId(id);
        return EntityToDTOConverter.convert(employeeRepository.save(employee));

    }

    public EmployeeDTO findEmployeeDTOById(Long id) {
        return EntityToDTOConverter.convert(employeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new));
    }

    public EmployeeDTO findTruncatedEmployeeDTOById(Long id) {
        return EntityToTruncatedDTOConverter.convert(employeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new));
    }

    public List<EmployeeDTO> findAll() {
        return employeeRepository.findAll().stream().map(EntityToDTOConverter::convert).collect(Collectors.toList());
    }

    public void deleteById(Long id) {
        // on fail throw DepartmentNotFoundException
//        departmentService.findDepartmentDTOById(id);
        final Optional<Employee> employeeRepositoryById = employeeRepository.findById(id);
        if (employeeRepositoryById.isPresent()) {
            if (nonNull(departmentService.findDepartmentDTOById(employeeRepositoryById.get().getDepartment().getId()))) {
                employeeRepository.deleteById(id);
            }

        }
    }
}
