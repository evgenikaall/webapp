package com.endava.service.converter;

import com.endava.model.entity.Department;
import org.springframework.stereotype.Component;
import com.endava.model.dto.TruncatedDepartmentDTO;
import org.springframework.core.convert.converter.Converter;

@Component
public class DepartmentEntityToTruncatedDepartmentDTOConverter implements Converter<Department, TruncatedDepartmentDTO> {
    @Override
    public TruncatedDepartmentDTO convert(Department department) {
        return TruncatedDepartmentDTO.builder()
                .name(department.getName())
                .build();
    }
}
