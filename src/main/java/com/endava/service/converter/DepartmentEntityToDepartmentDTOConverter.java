package com.endava.service.converter;

import com.endava.model.entity.Department;
import org.springframework.stereotype.Component;
import com.endava.model.dto.ExpandedDepartmentDTO;
import org.springframework.core.convert.converter.Converter;

@Component
public class DepartmentEntityToDepartmentDTOConverter implements Converter<Department, ExpandedDepartmentDTO> {
    @Override
    public ExpandedDepartmentDTO convert(Department department) {
        return ExpandedDepartmentDTO.builder()
                .name(department.getName())
                .manager(department.getManager())
                .locationId(department.getLocationId())
                .build();
    }
}
