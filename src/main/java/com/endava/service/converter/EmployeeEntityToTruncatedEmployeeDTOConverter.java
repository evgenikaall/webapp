package com.endava.service.converter;

import com.endava.model.entity.Employee;
import org.springframework.stereotype.Component;
import com.endava.model.dto.TruncatedEmployeeDTO;
import org.springframework.core.convert.converter.Converter;

@Component
public class EmployeeEntityToTruncatedEmployeeDTOConverter implements Converter<Employee, TruncatedEmployeeDTO> {

    @Override
    public TruncatedEmployeeDTO convert(Employee employee) {
        return TruncatedEmployeeDTO.builder()
                .firstName(employee.getFirstName())
                .lastName(employee.getLastName())
                .departmentId(employee.getDepartment().getId())
                .build();
    }
}
