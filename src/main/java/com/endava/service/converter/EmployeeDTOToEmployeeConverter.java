package com.endava.service.converter;

import lombok.RequiredArgsConstructor;
import com.endava.model.entity.Employee;
import com.endava.model.dto.ExpandedEmployeeDTO;
import org.springframework.stereotype.Component;
import org.springframework.core.convert.converter.Converter;

@Component
@RequiredArgsConstructor
public class EmployeeDTOToEmployeeConverter implements Converter<ExpandedEmployeeDTO, Employee> {

    @Override
    public Employee convert(ExpandedEmployeeDTO e) {
        return Employee.builder()
                .firstName(e.getFirstName())
                .lastName(e.getLastName())
                .email(e.getEmail())
                .phoneNumber(e.getPhoneNumber())
                .hireDate(e.getHireDate())
                .jobId(e.getJobId())
                .salary(e.getSalary())
                .commissionPCT(e.getCommissionPCT())
                .manager(e.getManager())
                .department(e.getDepartment())
                .build();
    }
}
