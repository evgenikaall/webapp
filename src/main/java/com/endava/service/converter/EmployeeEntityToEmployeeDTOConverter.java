package com.endava.service.converter;

import lombok.RequiredArgsConstructor;
import com.endava.model.entity.Employee;
import com.endava.model.entity.Department;
import org.springframework.stereotype.Component;
import com.endava.model.dto.ExpandedEmployeeDTO;
import com.endava.model.dto.ExpandedDepartmentDTO;
import org.springframework.core.convert.converter.Converter;

@Component
@RequiredArgsConstructor
public class EmployeeEntityToEmployeeDTOConverter implements Converter<Employee, ExpandedEmployeeDTO> {

    private final Converter<Department, ExpandedDepartmentDTO> converter;

    @Override
    public ExpandedEmployeeDTO convert(Employee employee) {
        return ExpandedEmployeeDTO.builder()
                .firstName(employee.getFirstName())
                .lastName(employee.getLastName())
                .email(employee.getEmail())
                .commissionPCT(employee.getCommissionPCT())
                .department(employee.getDepartment())
                .hireDate(employee.getHireDate())
                .jobId(employee.getJobId())
                .manager(employee.getManager())
                .phoneNumber(employee.getPhoneNumber())
                .salary(employee.getSalary())
                .build();
    }
}
