package com.endava.service.converter;

import com.endava.model.entity.Department;
import org.springframework.stereotype.Component;
import com.endava.model.dto.ExpandedDepartmentDTO;
import org.springframework.core.convert.converter.Converter;

@Component
public class DepartmentDTOToDepartmentEntityConverter implements Converter<ExpandedDepartmentDTO, Department> {
    @Override
    public Department convert(ExpandedDepartmentDTO expandedDepartmentDTO) {
        return Department.builder()
                .name(expandedDepartmentDTO.getName())
                .manager(expandedDepartmentDTO.getManager())
                .locationId(expandedDepartmentDTO.getLocationId())
                .build();
    }
}
