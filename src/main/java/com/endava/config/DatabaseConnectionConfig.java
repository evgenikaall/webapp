package com.endava.config;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.sql.Connection;
import java.sql.DriverManager;

@Configuration
@Profile("jdbc")
public class DatabaseConnectionConfig {

    @Bean
    @SneakyThrows
    public Connection jdbcConnection (
            @Value("${spring.datasource.url}") final String connectionUrl,
            @Value("${spring.datasource.username}") final String databaseUsername,
            @Value("${spring.datasource.password}") final String databasePassword ) {

        return DriverManager.getConnection(connectionUrl, databaseUsername, databasePassword);
    }
}
