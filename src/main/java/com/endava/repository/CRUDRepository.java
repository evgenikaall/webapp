package com.endava.repository;

import java.util.List;
import java.util.Optional;

public interface CRUDRepository<T, ID>{
    <S extends T> S save(S model);
    Optional<T> findById(ID id);
    List<T> findAll();
    void deleteById(ID id);
}
