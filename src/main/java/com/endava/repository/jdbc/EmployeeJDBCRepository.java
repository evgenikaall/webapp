package com.endava.repository.jdbc;

import com.endava.model.entity.Department;
import com.endava.model.entity.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.endava.repository.util.SQLQueryTemplates.TEMPLATE_QUERY_FOR_DELETING_EMPLOYEES;
import static com.endava.repository.util.SQLQueryTemplates.TEMPLATE_QUERY_FOR_DELETING_EMPLOYEE_FROM_JOB_HISTORY;
import static com.endava.repository.util.SQLQueryTemplates.TEMPLATE_QUERY_FOR_FINDING_EMPLOYEE;
import static com.endava.repository.util.SQLQueryTemplates.TEMPLATE_QUERY_FOR_INSERT_EMPLOYEES;
import static com.endava.repository.util.SQLQueryTemplates.TEMPLATE_QUERY_FOR_NEXT_EMPLOYEE_ID;
import static com.endava.repository.util.SQLQueryTemplates.TEMPLATE_QUERY_FOR_SELECT_ALL_EMPLOYEES;
import static com.endava.repository.util.SQLQueryTemplates.TEMPLATE_QUERY_FOR_SELECT_ALL_EMPLOYEES_AND_THEIR_DEPARTMENTS_AND_THEIR_MANAGERS;
import static com.endava.repository.util.SQLQueryTemplates.TEMPLATE_QUERY_FOR_UPDATING_DEPARTMENTS_WITH_NULL_MANAGER_IDS;
import static com.endava.repository.util.SQLQueryTemplates.TEMPLATE_QUERY_FOR_UPDATING_EMPLOYEE_FOR_NATIVE;
import static com.endava.repository.util.SQLQueryTemplates.TEMPLATE_QUERY_FOR_UPDATING_EMPLOYEES_WITH_NULL_MANAGERS_IDS;
import static java.util.Objects.isNull;

@Slf4j
@Repository
@Profile("jdbc")
public class EmployeeJDBCRepository extends JDBCRepository<Employee, Long> {

    public EmployeeJDBCRepository(Connection connection) {
        super(connection);
    }

    @Autowired
    private DepartmentJDBCRepository departmentJDBCRepository;

    @Override
    public Employee save(Employee employee) {

        if (isNull(employee)) {
            log.error("the given entity is null");
            throw new IllegalArgumentException("the given entity is null");
        }
        if (existRow(employee)) {
            return update(employee);
        } else {
            return persist(employee);
        }
    }

    @Override
    public Optional<Employee> findById(Long id) {
        if (isNull(id)) {
            log.error("the given id is null");
            throw new IllegalArgumentException("the given id is null");
        }
        try {
            final Statement statement = connection.createStatement();
            final ResultSet rs = statement.executeQuery(TEMPLATE_QUERY_FOR_FINDING_EMPLOYEE + id);
            if (rs.next()) {
                return Optional.of(buildEmployee(rs));
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage());
        }
        return Optional.empty();
    }

    @Override
    public List<Employee> findAll() {
        try {
            final Statement statement = connection.createStatement();
            final ResultSet rs = statement.executeQuery(TEMPLATE_QUERY_FOR_SELECT_ALL_EMPLOYEES_AND_THEIR_DEPARTMENTS_AND_THEIR_MANAGERS);
            final List<Employee> employeeList = new ArrayList<>();

            while (rs.next()) {
                employeeList.add(buildEmployee(rs));
            }
            return employeeList;

        } catch (SQLException ex) {
            log.error(ex.getMessage());
        }
        return Collections.emptyList();
    }

    @Override
    public void deleteById(Long id) {
        if (isNull(id)) {
            log.error("the given id is null");
            throw new IllegalArgumentException("the given id is null");
        }

        try {
            connection.setAutoCommit(false);
            final Statement statement = connection.createStatement();

            statement.executeUpdate(TEMPLATE_QUERY_FOR_DELETING_EMPLOYEE_FROM_JOB_HISTORY + id);
            statement.executeUpdate(TEMPLATE_QUERY_FOR_UPDATING_DEPARTMENTS_WITH_NULL_MANAGER_IDS + id);
            statement.executeUpdate(TEMPLATE_QUERY_FOR_UPDATING_EMPLOYEES_WITH_NULL_MANAGERS_IDS + id);
            statement.executeUpdate(TEMPLATE_QUERY_FOR_DELETING_EMPLOYEES + id);

            connection.commit();
        } catch (SQLException ex) {
            log.error(ex.getMessage());
        }
    }

    private boolean existRow(Employee employee) {
        if (isNull(employee.getId())) return false;
        try {
            final Statement statement = connection.createStatement();
            final ResultSet rs = statement.executeQuery(TEMPLATE_QUERY_FOR_SELECT_ALL_EMPLOYEES + employee.getId());
            return rs.next();
        } catch (SQLException ex) {
            log.error(ex.getMessage());
        }
        return false;
    }

    private Employee persist(Employee employee) {
        try {

            final PreparedStatement statement = connection.prepareStatement(TEMPLATE_QUERY_FOR_INSERT_EMPLOYEES);

            final ResultSet rs = connection.createStatement().executeQuery(TEMPLATE_QUERY_FOR_NEXT_EMPLOYEE_ID);

            rs.next();

            final long employeeId = rs.getLong(1);

            employee.setId(employeeId);

            statement.setLong(1, employeeId);
            setDataForPreparedStatement(statement, employee, false);

            int counterOfRows = statement.executeUpdate();

            if (counterOfRows > 0) return employee;

        } catch (SQLException ex) {
            log.error(ex.getMessage());
        }
        return null;
    }

    private Employee update(Employee employee) {
        try {
            final PreparedStatement statement = connection.prepareStatement(TEMPLATE_QUERY_FOR_UPDATING_EMPLOYEE_FOR_NATIVE + employee.getId());
            setDataForPreparedStatement(statement, employee, true);
            statement.executeUpdate();
            return employee;
        } catch (SQLException ex) {
            log.error(ex.getMessage());
        }
        return null;
    }

    private void setDataForPreparedStatement(PreparedStatement statement, Employee employee, Boolean forUpdate) throws SQLException {
        int u = forUpdate ? 1 : 0;

        if (isNull(employee.getFirstName())) {
            statement.setNull(2 - u, Types.VARCHAR);
        } else {
            statement.setString(2 - u, employee.getFirstName());
        }

        statement.setString(3 - u, employee.getLastName());
        statement.setString(4 - u, employee.getEmail());

        if (isNull(employee.getPhoneNumber())) {
            statement.setNull(5 - u, Types.VARCHAR);
        } else {
            statement.setString(5 - u, employee.getPhoneNumber());
        }

        statement.setDate(6 - u, employee.getHireDate());
        statement.setString(7 - u, employee.getJobId());
        if (isNull(employee.getSalary())) {
            statement.setNull(8 - u, Types.DECIMAL);
        } else {
            statement.setDouble(8 - u, employee.getSalary());
        }

        if (isNull(employee.getCommissionPCT())) {
            statement.setNull(9 - u, Types.DECIMAL);
        } else {
            statement.setDouble(9 - u, employee.getCommissionPCT());
        }

        if (isNull(employee.getManager()) || isNull(employee.getManager().getId())) {
            statement.setNull(10 - u, Types.BIGINT);
        } else {
            statement.setLong(10 - u, employee.getManager().getId());
        }

        if (isNull(employee.getDepartment()) || isNull(employee.getDepartment().getId())) {
            statement.setNull(11 - u, Types.BIGINT);
        } else {
            statement.setLong(11 - u, employee.getDepartment().getId());
        }
    }

    private Employee buildEmployee(ResultSet rs) throws SQLException {
        return Employee.builder()
                .id(rs.getLong(1))
                .firstName(rs.getString(2))
                .lastName(rs.getString(3))
                .email(rs.getString(4))
                .phoneNumber(rs.getString(5))
                .hireDate(rs.getDate(6))
                .jobId(rs.getString(7))
                .salary(rs.getDouble(8))
                .commissionPCT(rs.getDouble(9))
                .department(
                        Department.builder()
                                .id(rs.getLong(10))
                                .name(rs.getString(11))
                                .locationId(rs.getLong(12))
                                .build()
                )
                .manager(
                        Employee.builder()
                                .id(rs.getLong(13))
                                .firstName(rs.getString(14))
                                .lastName(rs.getString(15))
                                .email(rs.getString(16))
                                .phoneNumber(rs.getString(17))
                                .hireDate(rs.getDate(18))
                                .jobId(rs.getString(19))
                                .salary(rs.getDouble(20))
                                .commissionPCT(rs.getDouble(21))
                                .department(
                                        Department.builder()
                                        .id(rs.getLong(22))
                                        .name(rs.getString(23))
                                        .build()
                                )
                                .build()
                )
                .build();
    }
}
