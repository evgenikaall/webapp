package com.endava.repository.jdbc;

import com.endava.model.entity.Department;
import com.endava.model.entity.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.endava.repository.util.SQLQueryTemplates.TEMPLATE_QUERY_FOR_DELETING_DEPARTMENT;
import static com.endava.repository.util.SQLQueryTemplates.TEMPLATE_QUERY_FOR_FINDING_DEPARTMENT;
import static com.endava.repository.util.SQLQueryTemplates.TEMPLATE_QUERY_FOR_INSERTING_DEPARTMENT;
import static com.endava.repository.util.SQLQueryTemplates.TEMPLATE_QUERY_FOR_NEXT_DEPARTMENT_ID;
import static com.endava.repository.util.SQLQueryTemplates.TEMPLATE_QUERY_FOR_SELECT_ALL_DEPARTMENTS_AND_THEIR_MANAGERS;
import static com.endava.repository.util.SQLQueryTemplates.TEMPLATE_QUERY_FOR_UPDATING_DEPARTMENT;
import static com.endava.repository.util.SQLQueryTemplates.TEMPLATE_QUERY_FOR_UPDATING_EMPLOYEE_WITH_NULL_DEPARTMENT_IDS;
import static com.endava.repository.util.SQLQueryTemplates.TEMPLATE_QUERY_FOR_UPDATING_JOB_HISTORY_WITH_NULL_DEPARTMENT_IDS;
import static java.util.Objects.isNull;

@Slf4j
@Repository
@Profile("jdbc")
public class DepartmentJDBCRepository extends JDBCRepository<Department, Long> {

    public DepartmentJDBCRepository(Connection connection) {
        super(connection);
    }

    @Override
    public Department save(Department department) {

        if (isNull(department)) {
            log.error("the given entity is null");
            throw new IllegalArgumentException("the given entity is null");
        }
        if (existRow(department)) {
            return update(department);
        } else {
            return persist(department);
        }
    }

    @Override
    public Optional<Department> findById(Long id) {
        if (isNull(id)) {
            log.error("the given id is null");
            throw new IllegalArgumentException("the given id is null");
        }
        try {
            final Statement statement = connection.createStatement();
            final ResultSet rs = statement.executeQuery(TEMPLATE_QUERY_FOR_FINDING_DEPARTMENT + id);
            if (rs.next()) {
                return Optional.of(buildDepartment(rs));
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage());
        }
        return Optional.empty();
    }

    @Override
    public List<Department> findAll() {
        try {
            final Statement statement = connection.createStatement();
            final ResultSet rs = statement.executeQuery(TEMPLATE_QUERY_FOR_SELECT_ALL_DEPARTMENTS_AND_THEIR_MANAGERS);
            final List<Department> departments = new ArrayList<>();

            while (rs.next()) {
                departments.add(buildDepartment(rs));
            }

            return departments;

        } catch (SQLException ex) {
            log.error(ex.getMessage());
        }
        return Collections.emptyList();
    }


    @Override
    public void deleteById(Long id) {
        if (isNull(id)) {
            log.error("the given id is null");
            throw new IllegalArgumentException("the given id is null");
        }

        try {
            final Statement statement = connection.createStatement();
            connection.setAutoCommit(false);
            statement.executeUpdate(TEMPLATE_QUERY_FOR_UPDATING_EMPLOYEE_WITH_NULL_DEPARTMENT_IDS + id);
            statement.executeUpdate(TEMPLATE_QUERY_FOR_UPDATING_JOB_HISTORY_WITH_NULL_DEPARTMENT_IDS + id);
            statement.executeUpdate(TEMPLATE_QUERY_FOR_DELETING_DEPARTMENT + id);
            connection.commit();
        } catch (SQLException ex) {
            log.error(ex.getMessage());
        }
    }

    private boolean existRow(Department department) {
        if (isNull(department.getId())) return false;
        try {
            final Statement statement = connection.createStatement();
            final ResultSet rs = statement.executeQuery(TEMPLATE_QUERY_FOR_FINDING_DEPARTMENT + department.getId());
            return rs.next();
        } catch (SQLException ex) {
            log.error(ex.getMessage());
        }
        return false;
    }

    private Department persist(Department department) {
        try {

            final PreparedStatement statement = connection.prepareStatement(TEMPLATE_QUERY_FOR_INSERTING_DEPARTMENT);

            final ResultSet rs = connection.createStatement().executeQuery(TEMPLATE_QUERY_FOR_NEXT_DEPARTMENT_ID);

            rs.next();

            final long departmentID = rs.getLong(1);

            department.setId(departmentID);

            statement.setLong(1, departmentID);
            setPreparedStatement(department, statement, false);

            int counterOfRows = statement.executeUpdate();

            if (counterOfRows > 0) return department;

        } catch (SQLException ex) {
            log.error(ex.getMessage());
        }
        return null;
    }


    private Department update(Department department) {
        try {
            final PreparedStatement statement = connection.prepareStatement(TEMPLATE_QUERY_FOR_UPDATING_DEPARTMENT + department.getId());
            setPreparedStatement(department, statement, true);
            statement.executeUpdate();
            return department;
        } catch (SQLException ex) {
            log.error(ex.getMessage());
        }
        return null;
    }

    private Department buildDepartment(ResultSet rs) throws SQLException {
        return Department.builder()
                .id(rs.getLong(1))
                .name(rs.getString(2))
                .locationId(rs.getLong(3))
                .manager(
                        Employee.builder()
                                .id(rs.getLong(4))
                                .firstName(rs.getString(5))
                                .lastName(rs.getString(6))
                                .email(rs.getString(7))
                                .phoneNumber(rs.getString(8))
                                .hireDate(rs.getDate(9))
                                .jobId(rs.getString(10))
                                .salary(rs.getDouble(11))
                                .commissionPCT(rs.getDouble(12))
                                .build()
                )
                .build();
    }

    private void setPreparedStatement(Department department, PreparedStatement statement, Boolean forUpdate) throws SQLException {
        int u = forUpdate ? 1 : 0;
        statement.setString(2 - u, department.getName());
        if (isNull(department.getLocationId())) {
            statement.setNull(3 - u, Types.BIGINT);
        } else {
            statement.setLong(3 - u, department.getLocationId());
        }

        if (isNull(department.getManager()) || isNull(department.getManager().getId())) {
            statement.setNull(4 - u, Types.BIGINT);
        } else {
            statement.setLong(4 - u, department.getManager().getId());
        }
    }
}
