package com.endava.repository.jdbc;

import com.endava.repository.CRUDRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;

import java.sql.Connection;

@RequiredArgsConstructor
public abstract class JDBCRepository<T, ID> implements CRUDRepository<T, ID> {
    protected final Connection connection;
}
