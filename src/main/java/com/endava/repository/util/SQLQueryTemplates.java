package com.endava.repository.util;

public interface SQLQueryTemplates {


    String TEMPLATE_QUERY_FOR_FINDING_EMPLOYEE =
            "SELECT \n" +
            "e.EMPLOYEE_ID ,\n" +
            "e.FIRST_NAME ,\n" +
            "e.LAST_NAME ,\n" +
            "e.EMAIL ,\n" +
            "e.PHONE_NUMBER ,\n" +
            "e.HIRE_DATE ,\n" +
            "e.JOB_ID ,\n" +
            "e.SALARY ,\n" +
            "e.COMMISSION_PCT ,\n" +
            "e.DEPARTMENT_ID,\n" +
            "d.DEPARTMENT_NAME ,\n" +
            "d.LOCATION_ID ,\n" +
            "e.MANAGER_ID ,\n" +
            "m.FIRST_NAME ,\n" +
            "m.LAST_NAME ,\n" +
            "m.EMAIL ,\n" +
            "m.PHONE_NUMBER ,\n" +
            "m.HIRE_DATE ,\n" +
            "m.JOB_ID ,\n" +
            "m.SALARY ,\n" +
            "m.COMMISSION_PCT ,\n" +
            "m.DEPARTMENT_ID ,\n" +
            "d2.DEPARTMENT_NAME \n" +
            "FROM EMPLOYEES e \n" +
            "LEFT JOIN DEPARTMENTS d \n" +
            "ON e.DEPARTMENT_ID = d.DEPARTMENT_ID \n" +
            "LEFT JOIN EMPLOYEES m\n" +
            "ON e.MANAGER_ID = m.EMPLOYEE_ID\n" +
            "LEFT JOIN DEPARTMENTS d2\n" +
            "ON m.DEPARTMENT_ID = d2.DEPARTMENT_ID \n" +
            "WHERE e.EMPLOYEE_ID = ";


    String TEMPLATE_QUERY_FOR_SELECT_ALL_EMPLOYEES_AND_THEIR_DEPARTMENTS_AND_THEIR_MANAGERS =
            "SELECT \n" +
            "e.EMPLOYEE_ID ,\n" +
            "e.FIRST_NAME ,\n" +
            "e.LAST_NAME ,\n" +
            "e.EMAIL ,\n" +
            "e.PHONE_NUMBER ,\n" +
            "e.HIRE_DATE ,\n" +
            "e.JOB_ID ,\n" +
            "e.SALARY ,\n" +
            "e.COMMISSION_PCT ,\n" +
            "e.DEPARTMENT_ID,\n" +
            "d.DEPARTMENT_NAME ,\n" +
            "d.LOCATION_ID ,\n" +
            "e.MANAGER_ID ,\n" +
            "m.FIRST_NAME ,\n" +
            "m.LAST_NAME ,\n" +
            "m.EMAIL ,\n" +
            "m.PHONE_NUMBER ,\n" +
            "m.HIRE_DATE ,\n" +
            "m.JOB_ID ,\n" +
            "m.SALARY ,\n" +
            "m.COMMISSION_PCT ,\n" +
            "m.DEPARTMENT_ID ,\n" +
            "d2.DEPARTMENT_NAME \n" +
            "FROM EMPLOYEES e \n" +
            "LEFT JOIN DEPARTMENTS d \n" +
            "ON e.DEPARTMENT_ID = d.DEPARTMENT_ID \n" +
            "LEFT JOIN EMPLOYEES m\n" +
            "ON e.MANAGER_ID = m.EMPLOYEE_ID\n" +
            "LEFT JOIN DEPARTMENTS d2\n" +
            "ON m.DEPARTMENT_ID = d2.DEPARTMENT_ID ";

    String TEMPLATE_QUERY_FOR_DELETING_EMPLOYEE_FROM_JOB_HISTORY =
            "DELETE FROM JOB_HISTORY WHERE EMPLOYEE_ID = ";

    String TEMPLATE_QUERY_FOR_UPDATING_DEPARTMENTS_WITH_NULL_MANAGER_IDS =
            "UPDATE DEPARTMENTS SET MANAGER_ID = NULL WHERE MANAGER_ID = ";

    String TEMPLATE_QUERY_FOR_UPDATING_EMPLOYEES_WITH_NULL_MANAGERS_IDS =
            "UPDATE EMPLOYEES SET MANAGER_ID = NULL WHERE MANAGER_ID = ";

    String TEMPLATE_QUERY_FOR_DELETING_EMPLOYEES =
            "DELETE FROM EMPLOYEES WHERE EMPLOYEE_ID = ";

    String TEMPLATE_QUERY_FOR_SELECT_ALL_EMPLOYEES =
            "SELECT * FROM EMPLOYEES WHERE EMPLOYEE_ID = ";

    String TEMPLATE_QUERY_FOR_INSERT_EMPLOYEES =
            "INSERT INTO EMPLOYEES(EMPLOYEE_ID, FIRST_NAME, LAST_NAME, EMAIL, " +
                    "PHONE_NUMBER, HIRE_DATE, JOB_ID, SALARY, COMMISSION_PCT, MANAGER_ID, DEPARTMENT_ID) " +
                    "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    String TEMPLATE_QUERY_FOR_NEXT_EMPLOYEE_ID =
            "SELECT EMPLOYEES_SEQ.NEXTVAL FROM dual";

    String TEMPLATE_QUERY_FOR_UPDATING_EMPLOYEE_FOR_NATIVE =
            "UPDATE EMPLOYEES \n" +
                    "SET FIRST_NAME = ?,\n" +
                    "    LAST_NAME  = ?,\n" +
                    "    EMAIL = ?,\n" +
                    "    PHONE_NUMBER = ?,\n" +
                    "    HIRE_DATE = ?,\n" +
                    "    JOB_ID = ?,\n" +
                    "    SALARY = ?,\n" +
                    "    COMMISSION_PCT = ?,\n" +
                    "    MANAGER_ID = ?,\n" +
                    "    DEPARTMENT_ID = ?\n" +
                    "WHERE EMPLOYEE_ID = ";

    String TEMPLATE_QUERY_FOR_FINDING_DEPARTMENT =
            "SELECT \n" +
                    "d.DEPARTMENT_ID ,\n" +
                    "d.DEPARTMENT_NAME ,\n" +
                    "d.LOCATION_ID ,\n" +
                    "d.MANAGER_ID ,\n" +
                    "e.FIRST_NAME ,\n" +
                    "e.LAST_NAME ,\n" +
                    "e.EMAIL ,\n" +
                    "e.PHONE_NUMBER ,\n" +
                    "e.HIRE_DATE ,\n" +
                    "e.JOB_ID ,\n" +
                    "e.SALARY ,\n" +
                    "e.COMMISSION_PCT \n" +
                    "FROM DEPARTMENTS d \n" +
                    "LEFT JOIN EMPLOYEES e \n" +
                    "ON d.MANAGER_ID = e.EMPLOYEE_ID \n" +
                    "WHERE d.DEPARTMENT_ID = ";


    String TEMPLATE_QUERY_FOR_SELECT_ALL_DEPARTMENTS_AND_THEIR_MANAGERS =
            "SELECT \n" +
            "d.DEPARTMENT_ID ,\n" +
            "d.DEPARTMENT_NAME ,\n" +
            "d.LOCATION_ID ,\n" +
            "d.MANAGER_ID ,\n" +
            "e.FIRST_NAME ,\n" +
            "e.LAST_NAME ,\n" +
            "e.EMAIL ,\n" +
            "e.PHONE_NUMBER ,\n" +
            "e.HIRE_DATE ,\n" +
            "e.JOB_ID ,\n" +
            "e.SALARY ,\n" +
            "e.COMMISSION_PCT \n" +
            "FROM DEPARTMENTS d\n" +
            "LEFT JOIN EMPLOYEES e \n" +
            "ON d.MANAGER_ID = e.EMPLOYEE_ID";

    String TEMPLATE_QUERY_FOR_UPDATING_EMPLOYEE_WITH_NULL_DEPARTMENT_IDS =
            "UPDATE EMPLOYEES SET DEPARTMENT_ID = NULL WHERE DEPARTMENT_ID = ";

    String TEMPLATE_QUERY_FOR_UPDATING_JOB_HISTORY_WITH_NULL_DEPARTMENT_IDS =
            "UPDATE JOB_HISTORY SET DEPARTMENT_ID = NULL WHERE DEPARTMENT_ID = ";

    String TEMPLATE_QUERY_FOR_DELETING_DEPARTMENT =
            "DELETE FROM DEPARTMENTS WHERE DEPARTMENT_ID = ";

    String TEMPLATE_QUERY_FOR_INSERTING_DEPARTMENT =
            "INSERT INTO DEPARTMENTS (DEPARTMENT_ID, DEPARTMENT_NAME, LOCATION_ID, MANAGER_ID)\n" +
                    " VALUES (?, ?, ?, ?)";

    String TEMPLATE_QUERY_FOR_NEXT_DEPARTMENT_ID =
            "SELECT DEPARTMENTS_SEQ.NEXTVAL FROM dual";

    String TEMPLATE_QUERY_FOR_UPDATING_DEPARTMENT =
            "UPDATE DEPARTMENTS \n" +
                    "SET DEPARTMENT_NAME = ?\n" +
                    "    LOCATION_ID = ?\n" +
                    "    MANAGER_ID = ?\n" +
                    "WHERE DEPARTMENT_ID = ";

}
