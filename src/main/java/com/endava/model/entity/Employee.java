package com.endava.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.sql.Date;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.SEQUENCE;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "EMPLOYEES")
public class Employee {

    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "EMPLOYEES_SEQ")
    @SequenceGenerator(name = "EMPLOYEES_SEQ", sequenceName = "EMPLOYEES_SEQ", allocationSize = 1)
    @Column(name = "EMPLOYEE_ID")
    private Long id;

    @Column(name = "FIRST_NAME")
    @NotNull(message = "First Name must not be null")
    @NotBlank(message = "First Name must not be blank")
    @NotEmpty(message = "First Name must not be empty")
    private String firstName;

    @Column(name = "LAST_NAME")
    @NotNull(message = "Last Name must not be null")
    @NotBlank(message = "Last Name must not be blank")
    @NotEmpty(message = "Last Name must not be empty")
    private String lastName;

    @Email(message = "Incorrect email format")
    @Column(name = "EMAIL")
    @NotBlank(message = "Email must not be blank")
    private String email;

    @Column(name = "PHONE_NUMBER")
    @Pattern(regexp="0[1-9][0-9]{7}$", message = "Phone number must include only digits, start with 0 and have 9 digits")
    private String phoneNumber;

    @Column(name = "HIRE_DATE")
    private Date hireDate;

    // TODO enumerated
    @Column(name = "JOB_ID")
    private String jobId;

    @Min(value = 1, message = "Salary must be more then 1 or equal to 1")
    @Column(name = "SALARY")
    private Double salary;

    @Column(name = "COMMISSION_PCT")
    private Double commissionPCT;

    @OneToOne(cascade = ALL)
    @JoinColumn(name = "MANAGER_ID")
    private Employee manager;

    @ManyToOne(cascade = ALL)
    @JoinColumn(name = "DEPARTMENT_ID")
    private Department department;

}
