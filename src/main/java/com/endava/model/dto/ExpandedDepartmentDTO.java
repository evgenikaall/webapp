package com.endava.model.dto;

import com.endava.model.entity.Employee;
import lombok.Data;
import lombok.Builder;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExpandedDepartmentDTO implements DepartmentDTO {

    @NotNull(message = "Department name must not be null")
    @NotBlank(message = "Department name must not be blank")
    @NotEmpty(message = "Department name must not be empty")
    private String name;

    private Employee manager;
    private Long locationId;
}
