package com.endava.model.dto;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TruncatedDepartmentDTO implements DepartmentDTO {

    @NotNull(message = "Department name must not be null")
    @NotBlank(message = "Department name must not be blank")
    @NotEmpty(message = "Department name must not be empty")
    private String name;

}
