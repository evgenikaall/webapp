package com.endava.model.dto;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TruncatedEmployeeDTO implements EmployeeDTO {
    @NotNull(message = "First Name must not be null")
    @NotBlank(message = "First Name must not be blank")
    @NotEmpty(message = "First Name must not be empty")
    private String firstName;

    @NotNull(message = "Last Name must not be null")
    @NotBlank(message = "Last Name must not be blank")
    @NotEmpty(message = "Last Name must not be empty")
    private String lastName;

    private Long departmentId;
}
