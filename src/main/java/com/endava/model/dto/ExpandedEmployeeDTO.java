package com.endava.model.dto;

import com.endava.model.entity.Department;
import com.endava.model.entity.Employee;
import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExpandedEmployeeDTO implements EmployeeDTO {

    @NotNull(message = "First Name must not be null")
    @NotBlank(message = "First Name must not be blank")
    @NotEmpty(message = "First Name must not be empty")
    private String firstName;

    @NotNull(message = "Last Name must not be null")
    @NotBlank(message = "Last Name must not be blank")
    @NotEmpty(message = "Last Name must not be empty")
    private String lastName;

    @Email(message = "Incorrect email format")
    @NotBlank(message = "Email must not be blank")
    private String email;

    @Pattern(regexp="0[1-9][0-9]{7}$", message = "Phone number must include only digits, start with 0 and have 9 digits")
    private String phoneNumber;

    private Date hireDate;
    private String jobId;

    @Min(value = 1, message = "Salary must be more then 1 or equal to 1")
    private Double salary;
    private Double commissionPCT;
    private Employee manager;
    private Department department;
}
