package com.endava.controller;

import com.endava.model.dto.DepartmentDTO;
import com.endava.model.dto.ExpandedDepartmentDTO;
import com.endava.service.DepartmentService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("api/departments")
@RequiredArgsConstructor
@Api(value = "123")
public class DepartmentRestController {

    private final DepartmentService service;

    @GetMapping
    @ResponseStatus(OK)
    public List<DepartmentDTO> findAllDepartments(){
        return service.findAll();
    }

    @GetMapping("/{id}")
    @ResponseStatus(OK)
    public DepartmentDTO findEmployeeDTOById(@PathVariable(name = "id") Long id, @RequestHeader(value = "expand", defaultValue = "true") Boolean expand){
        if(expand){
            return service.findDepartmentDTOById(id);
        }else {
            return service.findTruncatedDepartmentDTOById(id);
        }
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public DepartmentDTO saveDepartment(@Valid @RequestBody ExpandedDepartmentDTO expandedDepartmentDTO){
        return service.save(expandedDepartmentDTO, null);
    }

    @PutMapping("/{id}")
    @ResponseStatus(OK)
    public DepartmentDTO updateEmployee(@Valid @RequestBody ExpandedDepartmentDTO expandedDepartmentDTO, @PathVariable("id") Long id){
        return service.save(expandedDepartmentDTO, id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteEmployee(@PathVariable("id") Long id){
        service.deleteById(id);
    }

}
