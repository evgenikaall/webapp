package com.endava.controller;

import com.endava.model.dto.EmployeeDTO;
import com.endava.model.dto.ExpandedEmployeeDTO;
import com.endava.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("api/employees")
@RequiredArgsConstructor
public class EmployeeRestController {

    private final EmployeeService service;

    @GetMapping
    @ResponseStatus(OK)
    public List<EmployeeDTO> findAllEmployees(){
        return service.findAll();
    }


    @GetMapping("/{id}")
    @ResponseStatus(OK)
    public EmployeeDTO findEmployeeDTOById(@PathVariable(name = "id") Long id, @RequestHeader("expand") Boolean expand){
            if(expand){
                return service.findEmployeeDTOById(id);
            }else {
                return service.findTruncatedEmployeeDTOById(id);
            }
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public EmployeeDTO saveEmployee(@Valid @RequestBody ExpandedEmployeeDTO expandedEmployeeDTO){
        return service.save(expandedEmployeeDTO, null);
    }

    // patch - just update
    // put - update. if not exist - save
    @PutMapping("/{id}")
    @ResponseStatus(OK)
    public EmployeeDTO updateEmployee(@Valid @RequestBody ExpandedEmployeeDTO expandedEmployeeDTO, @PathVariable("id") Long id){
        return service.save(expandedEmployeeDTO, id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteEmployee(@PathVariable("id") Long id){
        service.deleteById(id);
    }
}
