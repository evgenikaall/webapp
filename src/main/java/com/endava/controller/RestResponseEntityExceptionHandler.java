package com.endava.controller;

import com.endava.utils.exceptions.DepartmentNotFoundException;
import com.endava.utils.exceptions.EmployeeNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.singletonMap;

@RestControllerAdvice
public class RestResponseEntityExceptionHandler {
//
//    @ExceptionHandler(IllegalArgumentException.class)
//    public ResponseEntity<Object> handleIllegalArgumentExceptionOnGettingInfo(IllegalArgumentException ex) {
//        return ResponseEntity.badRequest().body(singletonMap("exception1", ex.getMessage()));
//    }
//
//    @ExceptionHandler(EmployeeNotFoundException.class)
//    public ResponseEntity<Object> handleEmployeeNotFoundException(EmployeeNotFoundException ex){
//        return ResponseEntity.badRequest().body(singletonMap("exception2", EmployeeNotFoundException.class.getSimpleName()));
//    }
//
//    @ExceptionHandler(DepartmentNotFoundException.class)
//    public ResponseEntity<Object> handleDepartmentNotFoundException(DepartmentNotFoundException ex){
//        return ResponseEntity.badRequest().body(singletonMap("exception3", DepartmentNotFoundException.class.getSimpleName()));
//
//    }
//
//    @ExceptionHandler(MethodArgumentNotValidException.class)
//    public ResponseEntity<Object> handleValidationExceptions(MethodArgumentNotValidException ex) {
//        Map<String, String> errors = new HashMap<>();
//        ex.getBindingResult().getAllErrors().forEach((error) -> {
//            String fieldName = ((FieldError) error).getField();
//            String errorMessage = error.getDefaultMessage();
//            errors.put(fieldName, errorMessage);
//        });
//        return ResponseEntity.badRequest().body(errors);
//    }
//

//    @ExceptionHandler(Exception.class)
//    public ResponseEntity<Object> handleAnyUnknownException(Exception ex) {
//        return ResponseEntity.badRequest().body(singletonMap("exception", ex.getClass().getSimpleName()));
//    }
}
