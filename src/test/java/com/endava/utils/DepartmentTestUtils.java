package com.endava.utils;

import com.endava.model.entity.Department;
import com.endava.model.dto.ExpandedDepartmentDTO;
import com.endava.model.dto.TruncatedDepartmentDTO;

public interface DepartmentTestUtils {
    Long TEST_DEPARTMENT_ID = 10L;

    Department TEST_DEPARTMENT =
            new Department(TEST_DEPARTMENT_ID, "Coffee's", null, null);

    Department TEST_DEPARTMENT_WITHOUT_ID =
            new Department(null, "Coffee's", null, null);

    ExpandedDepartmentDTO EXPANDED_TEST_DEPARTMENT_DTO =
            new ExpandedDepartmentDTO("Coffee's", null, null);

    TruncatedDepartmentDTO TRUNCATED_TEST_DEPARTMENT_DTO =
            new TruncatedDepartmentDTO("Coffee's");
}
