package com.endava.utils;

import com.endava.model.entity.Employee;
import com.endava.model.dto.ExpandedEmployeeDTO;
import com.endava.model.dto.TruncatedEmployeeDTO;

import static com.endava.utils.DepartmentTestUtils.TEST_DEPARTMENT;
import static com.endava.utils.DepartmentTestUtils.TEST_DEPARTMENT_ID;

public interface EmployeeTestUtils {

    Long TEST_EMPLOYEE_ID = 10L;

    Employee FULL_TEST_EMPLOYEE =
            new Employee(10L,
                    "John",
                    "Ivanov",
                    "jiva@gmail.com",
                    "012343212",
                    null,
                    "ST_MAN",
                    22000D,
                    null,
                    null,
                    TEST_DEPARTMENT);

    Employee TEST_EMPLOYEE_WITHOUT_ID =
            new Employee(null,
                    "John",
                    "Ivanov",
                    "jiva@gmail.com",
                    "012343212",
                    null,
                    "ST_MAN",
                    22000D,
                    null,
                    null,
                    TEST_DEPARTMENT);


    ExpandedEmployeeDTO EXPANDED_TEST_EMPLOYEE_DTO =
            new ExpandedEmployeeDTO("John",
                    "Ivanov",
                    "jiva@gmail.com",
                    "012343212",
                    null,
                    "ST_MAN",
                    22000D,
                    null,
                    null,
                    TEST_DEPARTMENT);

    TruncatedEmployeeDTO TRUNCATED_TEST_EMPLOYEE_DTO =
            new TruncatedEmployeeDTO("John",
                    "Ivanov",
                    TEST_DEPARTMENT_ID);


}
