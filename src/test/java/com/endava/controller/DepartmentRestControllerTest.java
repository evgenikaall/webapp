package com.endava.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.springframework.http.MediaType;
import com.endava.model.dto.DepartmentDTO;
import com.endava.service.DepartmentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;

import java.util.List;
import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static com.endava.utils.DepartmentTestUtils.TEST_DEPARTMENT_ID;
import static com.endava.utils.DepartmentTestUtils.EXPANDED_TEST_DEPARTMENT_DTO;
import static com.endava.utils.DepartmentTestUtils.TRUNCATED_TEST_DEPARTMENT_DTO;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class DepartmentRestControllerTest {

    private final static ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DepartmentService service;

    @AfterEach
    public void tearDown() {
        verifyNoMoreInteractions(service);
    }


    @Test
    public void shouldReturnAllRowsFromDepartmentDTOWithSuccessResponse() throws Exception {
        final List<DepartmentDTO> departmentsDTO = Collections.singletonList(EXPANDED_TEST_DEPARTMENT_DTO);

        when(service.findAll()).thenReturn(departmentsDTO);

        mockMvc.perform(get("/api/departments"))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(departmentsDTO)));

        verify(service).findAll();
    }

    @Test
    public void shouldReturnTruncatedEmployeeDTOOnFindingWithSuccessResponse() throws Exception {

        when(service.findTruncatedDepartmentDTOById(TEST_DEPARTMENT_ID)).thenReturn(TRUNCATED_TEST_DEPARTMENT_DTO);

        mockMvc.perform(get("/api/departments/10")
                .header("expand", false))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(TRUNCATED_TEST_DEPARTMENT_DTO)));

        verify(service).findTruncatedDepartmentDTOById(TEST_DEPARTMENT_ID);
    }

    @Test
    public void shouldReturnExpandEmployeeDTOOnFindingWithSuccessfullyResponse() throws Exception {

        when(service.findDepartmentDTOById(TEST_DEPARTMENT_ID)).thenReturn(EXPANDED_TEST_DEPARTMENT_DTO);

        mockMvc.perform(get("/api/departments/10")
                .header("expand", true))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(EXPANDED_TEST_DEPARTMENT_DTO)));

        verify(service).findDepartmentDTOById(TEST_DEPARTMENT_ID);
    }


    @Test
    public void shouldSaveNewEmployeeFromDTOWithCreatedResponseAndReturnItFromDataSource() throws Exception {

        when(service.save(EXPANDED_TEST_DEPARTMENT_DTO, null)).thenReturn(EXPANDED_TEST_DEPARTMENT_DTO);

        mockMvc.perform(post("/api/departments")
                .content(mapper.writeValueAsString(EXPANDED_TEST_DEPARTMENT_DTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(mapper.writeValueAsString(EXPANDED_TEST_DEPARTMENT_DTO)));

        verify(service).save(EXPANDED_TEST_DEPARTMENT_DTO, null);
    }

    @Test
    public void shouldUpdateEmployeeFromDTOByIdWithSuccessfullyResponseAndReturnUpdatedEmployeeDTOFromDataSource() throws Exception {

        when(service.save(EXPANDED_TEST_DEPARTMENT_DTO, TEST_DEPARTMENT_ID)).thenReturn(EXPANDED_TEST_DEPARTMENT_DTO);

        mockMvc.perform(put("/api/departments/10")
                .content(mapper.writeValueAsString(EXPANDED_TEST_DEPARTMENT_DTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(EXPANDED_TEST_DEPARTMENT_DTO)));

        verify(service).save(EXPANDED_TEST_DEPARTMENT_DTO, TEST_DEPARTMENT_ID);
    }

    @Test
    public void shouldDeleteEmployeeFromDataSourceByIdWithNoContentResponse() throws Exception {
        doNothing().when(service).deleteById(TEST_DEPARTMENT_ID);

        mockMvc.perform(delete("/api/departments/10"))
                .andExpect(status().isNoContent());

        verify(service).deleteById(TEST_DEPARTMENT_ID);
    }
}