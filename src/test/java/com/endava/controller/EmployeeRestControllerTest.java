package com.endava.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import com.endava.model.dto.EmployeeDTO;
import com.endava.service.EmployeeService;
import org.springframework.http.MediaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;

import java.util.List;
import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static com.endava.utils.EmployeeTestUtils.EXPANDED_TEST_EMPLOYEE_DTO;
import static com.endava.utils.EmployeeTestUtils.TEST_EMPLOYEE_ID;
import static com.endava.utils.EmployeeTestUtils.TRUNCATED_TEST_EMPLOYEE_DTO;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class EmployeeRestControllerTest {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeService service;

    @AfterEach
    public void tearDown() {
        verifyNoMoreInteractions(service);
    }

    @Test
    public void shouldReturnAllRowsFromEmployeeDTOWithSuccessResponse() throws Exception {
        final List<EmployeeDTO> employeesDTO = Collections.singletonList(EXPANDED_TEST_EMPLOYEE_DTO);

        when(service.findAll()).thenReturn(employeesDTO);

        mockMvc.perform(get("/api/employees"))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(employeesDTO)));

        verify(service).findAll();
    }

    @Test
    public void shouldReturnTruncatedEmployeeDTOOnFindingWithSuccessResponse() throws Exception {

        when(service.findTruncatedEmployeeDTOById(TEST_EMPLOYEE_ID)).thenReturn(TRUNCATED_TEST_EMPLOYEE_DTO);

        mockMvc.perform(get("/api/employees/10")
                .header("expand", false))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(TRUNCATED_TEST_EMPLOYEE_DTO)));

        verify(service).findTruncatedEmployeeDTOById(TEST_EMPLOYEE_ID);
    }

    @Test
    public void shouldReturnExpandEmployeeDTOOnFindingWithSuccessfullyResponse() throws Exception {

        when(service.findEmployeeDTOById(TEST_EMPLOYEE_ID)).thenReturn(EXPANDED_TEST_EMPLOYEE_DTO);

        mockMvc.perform(get("/api/employees/10")
                .header("expand", true))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(EXPANDED_TEST_EMPLOYEE_DTO)));

        verify(service).findEmployeeDTOById(TEST_EMPLOYEE_ID);
    }


    @Test
    public void shouldSaveNewEmployeeFromDTOWithCreatedResponseAndReturnItFromDataSource() throws Exception {

        when(service.save(EXPANDED_TEST_EMPLOYEE_DTO, null)).thenReturn(EXPANDED_TEST_EMPLOYEE_DTO);

        mockMvc.perform(post("/api/employees")
                .content(mapper.writeValueAsString(EXPANDED_TEST_EMPLOYEE_DTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(mapper.writeValueAsString(EXPANDED_TEST_EMPLOYEE_DTO)));

        verify(service).save(EXPANDED_TEST_EMPLOYEE_DTO, null);
    }

    @Test
    public void shouldUpdateEmployeeFromDTOByIdWithSuccessfullyResponseAndReturnUpdatedEmployeeDTOFromDataSource() throws Exception {

        when(service.save(EXPANDED_TEST_EMPLOYEE_DTO, TEST_EMPLOYEE_ID)).thenReturn(EXPANDED_TEST_EMPLOYEE_DTO);

        mockMvc.perform(put("/api/employees/10")
                .content(mapper.writeValueAsString(EXPANDED_TEST_EMPLOYEE_DTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(EXPANDED_TEST_EMPLOYEE_DTO)));

        verify(service).save(EXPANDED_TEST_EMPLOYEE_DTO, TEST_EMPLOYEE_ID);
    }

    @Test
    public void shouldDeleteEmployeeFromDataSourceByIdWithNoContentResponse() throws Exception {
        doNothing().when(service).deleteById(TEST_EMPLOYEE_ID);

        mockMvc.perform(delete("/api/employees/10"))
                .andExpect(status().isNoContent());

        verify(service).deleteById(TEST_EMPLOYEE_ID);
    }

}