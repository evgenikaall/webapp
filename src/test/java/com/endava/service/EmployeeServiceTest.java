package com.endava.service;


import com.endava.model.dto.ExpandedEmployeeDTO;
import com.endava.model.dto.TruncatedEmployeeDTO;
import com.endava.model.entity.Employee;
import com.endava.repository.CRUDRepository;
import com.endava.utils.exceptions.EmployeeNotFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.core.convert.converter.Converter;

import java.util.Collections;
import java.util.Optional;

import static com.endava.utils.DepartmentTestUtils.EXPANDED_TEST_DEPARTMENT_DTO;
import static com.endava.utils.DepartmentTestUtils.TEST_DEPARTMENT_ID;
import static com.endava.utils.EmployeeTestUtils.EXPANDED_TEST_EMPLOYEE_DTO;
import static com.endava.utils.EmployeeTestUtils.FULL_TEST_EMPLOYEE;
import static com.endava.utils.EmployeeTestUtils.TEST_EMPLOYEE_ID;
import static com.endava.utils.EmployeeTestUtils.TEST_EMPLOYEE_WITHOUT_ID;
import static com.endava.utils.EmployeeTestUtils.TRUNCATED_TEST_EMPLOYEE_DTO;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

class EmployeeServiceTest {

    @Mock
    private Converter<ExpandedEmployeeDTO, Employee> DTOToEntityConverter;

    @Mock
    private Converter<Employee, TruncatedEmployeeDTO> EntityToTruncatedDTOConverter;

    @Mock
    private Converter<Employee, ExpandedEmployeeDTO> EntityToDTOConverter;

    @Mock
    private CRUDRepository<Employee, Long> repository;

    @Mock
    private DepartmentService departmentService;

    @InjectMocks
    private EmployeeService employeeService;

    @AfterEach
    public void tearDown(){
        verifyNoMoreInteractions(DTOToEntityConverter, EntityToDTOConverter, EntityToTruncatedDTOConverter, repository, departmentService);
    }

    @Test
    public void shouldReturnEmployeeDTOOnSaving(){
        when(DTOToEntityConverter.convert(EXPANDED_TEST_EMPLOYEE_DTO)).thenReturn(TEST_EMPLOYEE_WITHOUT_ID);
        when(repository.save(TEST_EMPLOYEE_WITHOUT_ID)).thenReturn(FULL_TEST_EMPLOYEE);
        when(EntityToDTOConverter.convert(FULL_TEST_EMPLOYEE)).thenReturn(EXPANDED_TEST_EMPLOYEE_DTO);
        when(departmentService.findDepartmentDTOById(TEST_DEPARTMENT_ID)).thenReturn(EXPANDED_TEST_DEPARTMENT_DTO);

        assertEquals(EXPANDED_TEST_EMPLOYEE_DTO, employeeService.save(EXPANDED_TEST_EMPLOYEE_DTO, null));

        verify(repository).save(TEST_EMPLOYEE_WITHOUT_ID);
        verify(EntityToDTOConverter).convert(FULL_TEST_EMPLOYEE);
        verify(DTOToEntityConverter).convert(EXPANDED_TEST_EMPLOYEE_DTO);
        verify(departmentService).findDepartmentDTOById(TEST_EMPLOYEE_ID);

    }

    @Test
    public void shouldReturnEmployeeDTOOnUpdating(){
        when(DTOToEntityConverter.convert(EXPANDED_TEST_EMPLOYEE_DTO)).thenReturn(TEST_EMPLOYEE_WITHOUT_ID);
        when(repository.save(FULL_TEST_EMPLOYEE)).thenReturn(FULL_TEST_EMPLOYEE);
        when(EntityToDTOConverter.convert(FULL_TEST_EMPLOYEE)).thenReturn(EXPANDED_TEST_EMPLOYEE_DTO);
        when(departmentService.findDepartmentDTOById(TEST_DEPARTMENT_ID)).thenReturn(EXPANDED_TEST_DEPARTMENT_DTO);

        assertEquals(EXPANDED_TEST_EMPLOYEE_DTO, employeeService.save(EXPANDED_TEST_EMPLOYEE_DTO, TEST_EMPLOYEE_ID));

        verify(repository).save(TEST_EMPLOYEE_WITHOUT_ID);
        verify(EntityToDTOConverter).convert(FULL_TEST_EMPLOYEE);
        verify(DTOToEntityConverter).convert(EXPANDED_TEST_EMPLOYEE_DTO);
        verify(departmentService).findDepartmentDTOById(TEST_EMPLOYEE_ID);
    }

    @Test
    public void shouldReturnExpandedEmployeeDTOFoundedById(){
        when(repository.findById(TEST_EMPLOYEE_ID)).thenReturn(Optional.of(FULL_TEST_EMPLOYEE));
        when(EntityToDTOConverter.convert(FULL_TEST_EMPLOYEE)).thenReturn(EXPANDED_TEST_EMPLOYEE_DTO);

        assertEquals(EXPANDED_TEST_EMPLOYEE_DTO, employeeService.findEmployeeDTOById(TEST_EMPLOYEE_ID));

        verify(repository).findById(TEST_EMPLOYEE_ID);
        verify(EntityToDTOConverter).convert(FULL_TEST_EMPLOYEE);

    }

    @Test
    public void shouldThrowEmployeeNotFoundExceptionOnFindingExpandedEmployeeDTOById(){
        when(repository.findById(TEST_EMPLOYEE_ID)).thenThrow(EmployeeNotFoundException.class);

        assertThrows(EmployeeNotFoundException.class, () -> employeeService.findEmployeeDTOById(TEST_EMPLOYEE_ID));

        verify(repository).findById(TEST_EMPLOYEE_ID);

    }

    @Test
    public void shouldReturnTruncatedEmployeeDTOFoundedById(){
        when(repository.findById(TEST_EMPLOYEE_ID)).thenReturn(Optional.of(FULL_TEST_EMPLOYEE));
        when(EntityToTruncatedDTOConverter.convert(FULL_TEST_EMPLOYEE)).thenReturn(TRUNCATED_TEST_EMPLOYEE_DTO);

        assertEquals(TRUNCATED_TEST_EMPLOYEE_DTO, employeeService.findTruncatedEmployeeDTOById(TEST_EMPLOYEE_ID));

        verify(repository).findById(TEST_EMPLOYEE_ID);
        verify(EntityToTruncatedDTOConverter).convert(FULL_TEST_EMPLOYEE);
    }

    @Test
    public void shouldThrowEmployeeNotFoundExceptionOnFindingTruncateEmployeeDTOById(){
        when(repository.findById(TEST_EMPLOYEE_ID)).thenThrow(EmployeeNotFoundException.class);

        assertThrows(EmployeeNotFoundException.class, () -> employeeService.findTruncatedEmployeeDTOById(TEST_EMPLOYEE_ID));

        verify(repository).findById(TEST_EMPLOYEE_ID);

    }

    @Test
    public void shouldReturnSingletonListFromExpandedDTOsOnFindingAll(){
        when(repository.findAll()).thenReturn(Collections.singletonList(FULL_TEST_EMPLOYEE));
        when(EntityToDTOConverter.convert(FULL_TEST_EMPLOYEE)).thenReturn(EXPANDED_TEST_EMPLOYEE_DTO);

        assertEquals(Collections.singletonList(EXPANDED_TEST_EMPLOYEE_DTO), employeeService.findAll());

        verify(repository).findAll();
        verify(EntityToDTOConverter).convert(FULL_TEST_EMPLOYEE);
    }

    @Test
    public void shouldNotDoAnythingOnDeletingById(){
        doNothing().when(repository).deleteById(TEST_EMPLOYEE_ID);
        when(departmentService.findDepartmentDTOById(TEST_DEPARTMENT_ID)).thenReturn(EXPANDED_TEST_DEPARTMENT_DTO);


        employeeService.deleteById(TEST_EMPLOYEE_ID);

        verify(repository).deleteById(TEST_EMPLOYEE_ID);
        verify(departmentService).findDepartmentDTOById(TEST_EMPLOYEE_ID);
    }
}