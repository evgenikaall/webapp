package com.endava.service;


import com.endava.model.dto.ExpandedDepartmentDTO;
import com.endava.model.dto.TruncatedDepartmentDTO;
import com.endava.model.entity.Department;
import com.endava.repository.CRUDRepository;
import com.endava.utils.exceptions.EmployeeNotFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import java.util.Collections;
import java.util.Optional;

import static com.endava.utils.DepartmentTestUtils.EXPANDED_TEST_DEPARTMENT_DTO;
import static com.endava.utils.DepartmentTestUtils.TEST_DEPARTMENT;
import static com.endava.utils.DepartmentTestUtils.TEST_DEPARTMENT_ID;
import static com.endava.utils.DepartmentTestUtils.TEST_DEPARTMENT_WITHOUT_ID;
import static com.endava.utils.DepartmentTestUtils.TRUNCATED_TEST_DEPARTMENT_DTO;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

class DepartmentServiceTest {

    @Mock
    private CRUDRepository<Department, Long> repository;

    @Mock
    private Converter<Department, TruncatedDepartmentDTO> EntityToTruncatedDTOConverter;

    @Mock
    private Converter<Department, ExpandedDepartmentDTO> EntityToDTOConverter;

    @Mock
    private Converter<ExpandedDepartmentDTO, Department> DTOToEntityConverter;

    @InjectMocks
    private DepartmentService departmentService;

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(repository, EntityToTruncatedDTOConverter, EntityToDTOConverter, DTOToEntityConverter);
    }

    @Test
    void shouldReturnDepartmentDTOOnSaving() {
        when(DTOToEntityConverter.convert(EXPANDED_TEST_DEPARTMENT_DTO)).thenReturn(TEST_DEPARTMENT_WITHOUT_ID);
        when(repository.save(TEST_DEPARTMENT_WITHOUT_ID)).thenReturn(TEST_DEPARTMENT);
        when(EntityToDTOConverter.convert(TEST_DEPARTMENT)).thenReturn(EXPANDED_TEST_DEPARTMENT_DTO);

        assertEquals(EXPANDED_TEST_DEPARTMENT_DTO, departmentService.save(EXPANDED_TEST_DEPARTMENT_DTO, null));

        verify(DTOToEntityConverter).convert(EXPANDED_TEST_DEPARTMENT_DTO);
        verify(repository).save(TEST_DEPARTMENT_WITHOUT_ID);
        verify(EntityToDTOConverter).convert(TEST_DEPARTMENT);

    }


    @Test
    public void shouldReturnDepartmentDTOOnUpdating() {
        when(DTOToEntityConverter.convert(EXPANDED_TEST_DEPARTMENT_DTO)).thenReturn(TEST_DEPARTMENT);
        when(repository.save(TEST_DEPARTMENT)).thenReturn(TEST_DEPARTMENT);
        when(EntityToDTOConverter.convert(TEST_DEPARTMENT)).thenReturn(EXPANDED_TEST_DEPARTMENT_DTO);

        assertEquals(EXPANDED_TEST_DEPARTMENT_DTO, departmentService.save(EXPANDED_TEST_DEPARTMENT_DTO, TEST_DEPARTMENT_ID));

        verify(repository).save(TEST_DEPARTMENT);
        verify(EntityToDTOConverter).convert(TEST_DEPARTMENT);
        verify(DTOToEntityConverter).convert(EXPANDED_TEST_DEPARTMENT_DTO);

    }

    @Test
    public void shouldReturnExpandedDepartmentDTOFoundedById() {
        when(repository.findById(TEST_DEPARTMENT_ID)).thenReturn(Optional.of(TEST_DEPARTMENT));
        when(EntityToDTOConverter.convert(TEST_DEPARTMENT)).thenReturn(EXPANDED_TEST_DEPARTMENT_DTO);

        assertEquals(EXPANDED_TEST_DEPARTMENT_DTO, departmentService.findDepartmentDTOById(TEST_DEPARTMENT_ID));

        verify(repository).findById(TEST_DEPARTMENT_ID);
        verify(EntityToDTOConverter).convert(TEST_DEPARTMENT);

    }

    @Test
    public void shouldThrowDepartmentNotFoundExceptionOnFindingExpandedEmployeeDTOById() {
        when(repository.findById(TEST_DEPARTMENT_ID)).thenThrow(EmployeeNotFoundException.class);

        assertThrows(EmployeeNotFoundException.class, () -> departmentService.deleteById(TEST_DEPARTMENT_ID));

        verify(repository).findById(TEST_DEPARTMENT_ID);
    }

    @Test
    public void shouldReturnTruncatedEmployeeDTOFoundedById() {
        when(repository.findById(TEST_DEPARTMENT_ID)).thenReturn(Optional.of(TEST_DEPARTMENT));
        when(EntityToTruncatedDTOConverter.convert(TEST_DEPARTMENT)).thenReturn(TRUNCATED_TEST_DEPARTMENT_DTO);

        assertEquals(TRUNCATED_TEST_DEPARTMENT_DTO, departmentService.findTruncatedDepartmentDTOById(TEST_DEPARTMENT_ID));

        verify(repository).findById(TEST_DEPARTMENT_ID);
        verify(EntityToTruncatedDTOConverter).convert(TEST_DEPARTMENT);
    }

    @Test
    public void shouldThrowDepartmentNotFoundExceptionOnFindingTruncatedDepartmentDTOById() {
        when(repository.findById(TEST_DEPARTMENT_ID)).thenThrow(EmployeeNotFoundException.class);

        assertThrows(EmployeeNotFoundException.class, () -> departmentService.findTruncatedDepartmentDTOById(TEST_DEPARTMENT_ID));

        verify(repository).findById(TEST_DEPARTMENT_ID);

    }

    @Test
    public void shouldReturnSingletonListFromExpandedDTOsOnFindingAll() {
        when(repository.findAll()).thenReturn(Collections.singletonList(TEST_DEPARTMENT));
        when(EntityToDTOConverter.convert(TEST_DEPARTMENT)).thenReturn(EXPANDED_TEST_DEPARTMENT_DTO);

        assertEquals(Collections.singletonList(EXPANDED_TEST_DEPARTMENT_DTO), departmentService.findAll());

        verify(repository).findAll();
        verify(EntityToDTOConverter).convert(TEST_DEPARTMENT);
    }

    @Test
    public void shouldNotDoAnythingOnDeletingById() {
        when(repository.findById(TEST_DEPARTMENT_ID)).thenReturn(Optional.of(TEST_DEPARTMENT));
        when(EntityToDTOConverter.convert(TEST_DEPARTMENT)).thenReturn(EXPANDED_TEST_DEPARTMENT_DTO);

        departmentService.deleteById(TEST_DEPARTMENT_ID);

        verify(repository).deleteById(TEST_DEPARTMENT_ID);
        verify(repository).findById(TEST_DEPARTMENT_ID);
        verify(EntityToDTOConverter).convert(TEST_DEPARTMENT);

    }
}